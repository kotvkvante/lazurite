#ifndef ATOMS_PARAMS_H
#define ATOMS_PARAMS_H

#include "cf_atoms.h"

#define NO_OS 0

#ifdef ATOMS_PARAMS_IMPL
const int DEFAULT_OXIDATION_STATES[ATOMS_COUNT] = {
    [NOT_ATOM]        = 0,
    [ATOM_HYDROGEN]   = 1,
    [ATOM_HELIUM]     = 0,
    [ATOM_LITHIUM]    = 1,
    [ATOM_BERYLLIUM]  = 2,
    [ATOM_BORON]      = 3,
    [ATOM_CARBON]     = 4,
    [ATOM_NITROGEN]   = 0,
    [ATOM_OXYGEN]     = -2,
    [ATOM_FLUORINE]   = -1,
    [ATOM_NEON]       = 0,
    [ATOM_SODIUM]     = 1,
    [ATOM_MAGNESIUM]  = 2,
    [ATOM_ALUMINIUM]  = 3,
    [ATOM_SILICON]    = 4,
    [ATOM_PHOSPHORUS] = 5,
    [ATOM_SULFUR]     = 4,
    [ATOM_CHLORINE]   = -1,
    [ATOM_ARGON]      = 0
};

const float DEFAULT_ATOMIC_MASSES[] = {
    [NOT_ATOM       ] = 0,
    [ATOM_HYDROGEN  ] = 1.0078, // 1,
    [ATOM_HELIUM    ] = 4.0026,
    [ATOM_LITHIUM   ] = 6.9410,
    [ATOM_BERYLLIUM ] = 9.0122,
    [ATOM_BORON     ] = 10.811,
    [ATOM_CARBON    ] = 12.011, // 12,
    [ATOM_NITROGEN  ] = 14.007, // 14,
    [ATOM_OXYGEN    ] = 15.999, // 16,
    [ATOM_FLUORINE  ] = 18.998,
    [ATOM_NEON      ] = 20.180,
    [ATOM_SODIUM    ] = 22.990,
    [ATOM_MAGNESIUM ] = 24.305,
    [ATOM_ALUMINIUM ] = 26.982,
    [ATOM_SILICON   ] = 28.086,
    [ATOM_PHOSPHORUS] = 30.974,
    [ATOM_SULFUR    ] = 32.065,
    [ATOM_CHLORINE  ] = 35.453,
    [ATOM_ARGON     ] = 39.948,
    [ATOM_CALCIUM   ] = 40.08,
    [ATOM_SCANDIUM  ] = 44.9559,
    [ATOM_TITANIUM  ] = 47.90,
    [ATOM_VANADIUM  ] = 50.9415,
    [ATOM_CHROMIUM  ] = 51.996,
    [ATOM_MANGANESE ] = 54.9380,
    [ATOM_IRON      ] = 55.847,
    [ATOM_NICKEL    ] = 58.70,
    [ATOM_COBALT    ] = 58.9332,
    [ATOM_COPPER    ] = 63.546,
    [ATOM_ZINC      ] = 65.38,
    [ATOM_GALLIUM   ] = 69.72,
    [ATOM_GERMANIUM ] = 72.59,
    [ATOM_ARSENIC   ] = 74.9216,
    [ATOM_SELENIUM  ] = 78.96,
    [ATOM_BROMINE   ] = 79.904,
    [ATOM_KRYPTON   ] = 83.80,
    [ATOM_RUBIDIUM  ] = 85.4678,
    [ATOM_STRONTIUM ] = 87.62,
    [ATOM_YTTRIUM   ] = 88.9059,
    [ATOM_ZIRCONIUM ] = 91.22,
    [ATOM_NIOBIUM   ] = 92.9064,
    [ATOM_MOLYBDENUM] = 95.95,
    [ATOM_TECHNETIUM] = 96.905,
    [ATOM_RUTHENIUM ] = 101.07,
    [ATOM_RHODIUM   ] = 102.9055,
    [ATOM_PALLADIUM ] = 106.4,
    [ATOM_SILVER    ] = 107.868,
    [ATOM_CADMIUM   ] = 112.41,
    [ATOM_INDIUM    ] = 114.82,
    [ATOM_TIN       ] = 118.69,
    [ATOM_ANTIMONY  ] = 121.75,
    [ATOM_IODINE    ] = 126.9045,
};

char* ATOMIC_SYMBOLS[] = {
    [NOT_ATOM]        = "NA",
    [ATOM_HYDROGEN]   = "H",
    [ATOM_HELIUM]     = "He",
    [ATOM_LITHIUM]    = "Li",
    [ATOM_BERYLLIUM]  = "Be",
    [ATOM_BORON]      = "B",
    [ATOM_CARBON]     = "C",
    [ATOM_NITROGEN]   = "N",
    [ATOM_OXYGEN]     = "O",
    [ATOM_FLUORINE]   = "F",
    [ATOM_NEON]       = "Ne",
    [ATOM_SODIUM]     = "Na",
    [ATOM_MAGNESIUM]  = "Mg",
    [ATOM_ALUMINIUM]  = "Al",
    [ATOM_SILICON]    = "Si",
    [ATOM_PHOSPHORUS] = "P",
    [ATOM_SULFUR]     = "S",
    [ATOM_CHLORINE]   = "Cl",
    [ATOM_ARGON]      = "Ar"
};

float ATOMIC_MASSES[ATOMS_COUNT];
int   OXIDATION_STATES[ATOMS_COUNT];


#endif

extern float ATOMIC_MASSES[ATOMS_COUNT];
extern int   OXIDATION_STATES[ATOMS_COUNT];

extern const float DEFAULT_ATOMIC_MASSES[ATOMS_COUNT];
extern const int   DEFAULT_OXIDATION_STATES[ATOMS_COUNT];

extern char* ATOMIC_SYMBOLS[];


#endif /* end of include guard: ATOMS_PARAMS_H */
