#include <stdlib.h>
#include <stdio.h>

#include "chemform.h"

int main(int argc, char const *argv[]) {
    UNUSED(argc);
    UNUSED(argv);
    
    cf_init_default();

    molecule_t* h2o = molecule_from_ai_arrays(
        2,
        AARRAY{ATOM_HYDROGEN, ATOM_OXYGEN},
        IARRAY{2, 1}
    );

    molecule_t* co2 = molecule_from_ai_arrays(
        2,
        AARRAY{ATOM_CARBON, ATOM_OXYGEN},
        IARRAY{1, 2}
    );

    molecule_t* ch4 = molecule_from_ai_arrays(
        2,
        AARRAY{ATOM_CARBON, ATOM_HYDROGEN},
        IARRAY{1, 4}
    );

    molecule_print(h2o);
    molecule_print(co2);
    molecule_print(ch4);

    molecule_t* o2 = molecule_from_ai_arrays(
        1,
        AARRAY{ATOM_OXYGEN},
        IARRAY{2}
    );
    molecule_t* h2 = molecule_from_ai_arrays(
        1, AARRAY{ATOM_HYDROGEN},
        IARRAY{2}
    );

    molecule_t* n2o4 = molecule_from_ai_arrays(
        2, AARRAY{ATOM_NITROGEN, ATOM_OXYGEN},
        IARRAY{2, 4});
    molecule_t* n2h4 = molecule_from_ai_arrays(
        2, AARRAY{ATOM_NITROGEN, ATOM_HYDROGEN},
        IARRAY{2, 4});

    printf("km0 = %f\n", km0(h2, o2));
    printf("km0 = %f\n", km0(n2o4, n2h4));

    molecule_t* air = molecule_from_ai_arrays(
        4, AARRAY{ATOM_NITROGEN, ATOM_OXYGEN, ATOM_ARGON, ATOM_CARBON},
        IARRAY{53.916, 14.487, 0.323, 0.011}
    );
    molecule_t* gasoline = molecule_from_ai_arrays(
        2, AARRAY{ATOM_CARBON, ATOM_HYDROGEN},
        IARRAY{7.21, 13.29}
    );
    printf("km0 = %f\n", km0(air, gasoline));

    molecule_t* mix = molecule_from_ai_arrays(
        6, AARRAY{ATOM_HYDROGEN, ATOM_CARBON, ATOM_NITROGEN, ATOM_OXYGEN, ATOM_ALUMINIUM, ATOM_CHLORINE},
        IARRAY{60.4, 9.9, 11.3, 17, 10.0, 3.4}
    );
    molecule_print(mix);
    molecule_print(air);

    printf("km0 = %f\n", km0(air, mix));

    return 0;
}
