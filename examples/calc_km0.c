#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <argp.h>

#include "chemform.h"

#define DELIMS ", "
#define MAX_SUBSTANCES_COUNT 4

void parse_component(char* str, char** formula, float* mf)
{
    char* mfs;
    char* fs;
    fs = strtok(str, DELIMS);
    mfs = strtok(NULL, DELIMS);

    *formula = fs;
    sscanf(mfs, "%f", mf);
}

void read_component(component_t* comp)
{
    size_t n;
    printf("Count of substances in component: ");

    scanf("%zu", &n);
    getchar(); // read newline

    if (n > MAX_SUBSTANCES_COUNT) {
        printf("Substances count must be less than %d and grater than 0. Given: %lu", MAX_SUBSTANCES_COUNT, n);
        exit(-1);
    }

    float* mass_fractions = malloc(sizeof(float) * n);
    char** formulas       = malloc(sizeof(char*) * n);
    printf("Substances formulas and their mass fractions: \n");
    for (size_t i = 0; i < n; i++) {
        char *string = NULL;
        size_t size = 0;
        ssize_t chars_read;

        printf("> ");
        chars_read = getline(&string, &size, stdin);
        parse_component(string, &formulas[i], &mass_fractions[i]);
    }

    for (size_t i = 0; i < n; i++) {
        printf("%s [%f]\n", formulas[i], mass_fractions[i]);
    }

    component_init_from_s_mf(comp, n,
        formulas,
        mass_fractions
    );
}

int main(int argc, char const *argv[])
{
    cf_init_default();

    component_t* fl = component_new();
    component_t* ox = component_new();

    printf("Enter fuel:\n");
    read_component(fl);
    printf("Enter oxidizer:\n");
    read_component(ox);

    component_calc_scf(fl);
    component_calc_scf(ox);

    float km0 = cf_calc_km0_cc(fl, ox);

    printf("Km0 = %f\n", km0);
    return 0;
}
