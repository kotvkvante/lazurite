Release
```
mkdir _build
cd _build
cmake ../
make

./main
> or
./main2

```
Debug:
```
mkdir _debug
cd _debug
cmake -DCMAKE_BUILD_TYPE=Debug ../
make

gdb -q ./main
> or
gdb -q ./main2
```

[*] Нужно добавить возможность менять условную химическую массу УХФ (100->1000->100)
[*] Переименовтаь masses_fraction -> mass_fractions
