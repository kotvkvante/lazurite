#ifndef LZ_PARSE_H
#define LZ_PARSE_H

char* parse_index(char* f, float* index);
char* parse_atom(char* f);
char* parse_brackets(char* f);
void parse_molecule(char* f);

#endif /* end of include guard: LZ_PARSE_H */
