#include <stdlib.h>
#include <stdio.h>

#include "chemform.h"
#include "cf_types.h"
#include "cf_array.h"
#include "cf_parse.h"

int main(int argc, char const *argv[]) {
    UNUSED(argc);
    UNUSED(argv);

    cf_init_default();

    // molecule_t* air = molecule_new();
    // molecule_init_from_str(air, "N53.916O14.487Ar0.323C0.011");
    // molecule_print(air);
    component_t* air = component_new();
    component_init_from_s_mf(air, 1,
        STRARR{"N53.916O14.487Ar0.323C0.011"},
        FRCARR{1.0}
    );
    component_calc_scf(air);
    component_print(air);
    component_print_scf(air);

    // float k = km0(
    //     air, component_get_scf(c)
    // );
    // printf("> Km0 = %f\n", k);

    component_t* af = component_new();
    component_init_from_s_mf(af, 3,
        STRARR{"C5H18B10O", "C10H16", "C7H8"},
        FRCARR{0.8, 0.1, 0.1});
    component_calc_scf(af);
    component_print(af);
    component_print_scf(af);

    float k1 = km0(
        component_get_scf(air), component_get_scf(af)
    );
    printf("> Km0 = %f\n", k1);
    //
    // molecule_t* fuel = molecule_new();
    // molecule_init_from_str(fuel, "C3.48H9.17O0.39B3.96");
    // float k2 = km0(
    //     air, fuel
    // );
    // printf("> Km0 = %f\n", k2);



    // float res[4*3] = {0};
    // float m1[4*2] = {
    //     1, 2,
    //     3, 4,
    //     5, 6,
    //     7, 8
    // };
    // float m2[2*3] = {
    //     9, 10, 11,
    //     12, 13, 14
    // };
    // float res[4*3] = {0};
    // float m1[4*2] = {
    //     1, 2,
    //     3, 4,
    //     5, 6,
    //     7, 8
    // };
    // float m2[2*3] = {
    //     9, 10, 11,
    //     12, 13, 14
    // };
    //
    // printf("%p\n", (void*)res);
    // printf("%p\n", (void*)m1);
    // printf("%p\n", (void*)m2);
    //
    // math_matrix_multiply(
    //     res,
    //     m1, 4, 2,
    //     m2, 2, 3
    // );

    return 0.0f;
}
