#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <argp.h>

#include "chemform.h"

#define COUNT 3
#define DELIMS ", \n"
#define OK 0
#define NOT_OK -1

const char *argp_program_version = "3components v1.1";
const char *argp_program_bug_address = "{ yourself =/ }";
static char doc[] = "This Program is used to calculate scf of 3 molecules.";
static char args_doc[] = "";
static struct argp_option options[] = {
    { "mass-fractions", 'm', "", 0, "String of mass fractions for each molecule separated by space or comma. e.g. \"0.2 0.1 0.7\"", 0},
    { "formulas",       'f', "", 0, "String of formulas for each molecule separated by space or comma. e.g. \"H2 O2 CH4\"", 0},
    { "cmm",            'c', "", 0, "Conventional molar mass (cmm). Default value is 100.0f.", 1},
    { "in-file",  'i', "FILE", 0, "Input file", 2},
    { "out-file", 'o', "FILE", 0, "Output file", 2},
    { 0 }
};

struct arguments {
    char* formulas;
    char* mass_fractions;
    char* cmm;
    char* in_file;
    char* out_file;
};

// Нужно: сделать обработку аргументов in-file и out-file
static error_t parse_opt(int key, char *arg, struct argp_state *state) {
    struct arguments *arguments = state->input;
    switch (key) {
        case 'o': arguments->out_file       = arg; break;
        case 'i': arguments->in_file        = arg; break;
        case 'f': arguments->formulas       = arg; break;
        case 'm': arguments->mass_fractions = arg; break;
        case 'c': arguments->cmm            = arg; break;
        case ARGP_KEY_ARG: return 0;
        case ARGP_KEY_END: {
            // Нужно: сделать обработку заданных параметров
            // bool t1 = (arguments->mass_fractions==NULL) && (arguments->formulas==NULL);
            // bool t2 = (arguments->file==NULL);
            // if (t1 == t2)
            // {
            //     argp_failure(state, 1, 0, "required (-f && -m) or -i. See --help for more information");
            //     exit(ARGP_ERR_UNKNOWN);
            // }
        } break;
        default: return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

static struct argp argp = { options, parse_opt, args_doc, doc, 0, 0, 0 };

static int arg_parse_formulas(struct arguments* arguments, char** formulas)
{
    int n1 = 0;
    char* fs = NULL;

    fs = strtok(arguments->formulas, DELIMS);
    while ((fs!=NULL) && (n1 <= 2))
    {
        formulas[n1] = fs;
        fs = strtok(NULL, ", ");
        n1++;
    }
    if (n1 != 3) {
        printf("Need exactly 3 formulas!\n");
        return NOT_OK;
    }

    return OK;
}

static int arg_parse_mass_fractions(struct arguments* arguments, float* mass_fractions)
{
    int n2 = 0;
    char* fms = arguments->mass_fractions;
    char* str_end = NULL;

    fms = strtok(arguments->mass_fractions, DELIMS);
    while ((fms!=NULL) && (n2 <= 2))
    {
        float f = strtof(fms, &str_end);
        if (fms == str_end) break;
        mass_fractions[n2] = f;
        fms = strtok(NULL, ", ");
        n2++;
    }
    if (n2 != 3) { printf("Need 3 floats!\n"); return NOT_OK; };
    return OK;
}


int main(int argc, char *argv[])
{
    struct arguments arguments;

    arguments.in_file  = NULL;
    arguments.out_file = NULL;

    arguments.formulas = NULL;
    arguments.mass_fractions = NULL;
    arguments.cmm = NULL;

    argp_parse(&argp, argc, argv, 0, 0, &arguments);

    if (arguments.cmm) {
        float cmm;
        if(sscanf(arguments.cmm, "%f", &cmm) <= 0) {
            printf("Failed to convert -c (--cmm) option argument to float. Given: %s\n", arguments.cmm);
            exit(NOT_OK);
        }
        cf_set_cmm(cmm);
    }

    char* formulas[COUNT];
    float mass_fractions[COUNT];
    if (arguments.formulas && arguments.mass_fractions)
    {
        if (arg_parse_formulas(&arguments, formulas) < 0) {
            printf("Failed to parse formulas.\n");
            exit(NOT_OK);
        }

        if (arg_parse_mass_fractions(&arguments, mass_fractions) < 0)
        {
            printf("Failed to parse mass fractions.\n");
            exit(NOT_OK);
        }


        cf_init_default();
        component_t* af = component_new();
        component_init_from_s_mf(af, COUNT,
            formulas, mass_fractions
        );
        component_calc_scf(af);
        component_print(af);
        component_print_scf(af);
    }
}
