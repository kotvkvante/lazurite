#ifndef LAZURITE_TYPES_H
#define LAZURITE_TYPES_H

#include <stdint.h>
#include <stdlib.h>

#include "cf_atoms.h"

typedef struct bounded_atom_t {
    atom_t atom;
    // Можно проводить расчёт при использовании дробных степеней окисления?
    int oxidation_state;
    // Индексы
    float index;
} bounded_atom_t;

typedef struct molecule_t {
    bounded_atom_t* bounded_atoms_array;
    size_t bounded_atoms_count;
    float molar_mass;
    // Заданное представление молекулы: "NH4NO3" -> "NH4NO3"
    //                                  "(NH4)2SO4" -> "(NH4)2SO4"
    char* str;
    // Нужно: написать функцию, определяющую
    // упрощённое представление молекулы.
    // Упрощённое представление молекулы: "NH4NO3" -> "H4O3N2"
    //                                    "(NH4)2SO4" -> "H8N2O4S"
    char* str_short;
} molecule_t;

typedef enum {
    COMPONENT_UNDEFINED,
    COMPONENT_OXIDIZER,
    COMPONENT_FUEL,
    COMPONENT_HUBRID
} component_type_t;

typedef struct component_t{
    component_type_t type;
    // Использовании указателя может приводить к ошибке.
    // При освобождении памяти молекулы, входящей в состав компонента,
    // указатель внутри структуры становится неопределённым.
    // Можно: делать собственную копию молекулы для компонента.
    // -> Больше используемый объём памяти.
    size_t      molecules_count;
    molecule_t** molecules_array;
    float*      masses_fraction;
    // specific chemical formula <=> удельная/условая химическая формула
    molecule_t  scf;
    // potential <=> потенциал
    float phi;

} component_t;

// typedef struct fuel_t {
//     component_t* components_array;
//     size_t       components_count;
//     molecule_t   scf; // specific chemical formula
//     float        phi;
// } fuel_t;

// typedef struct oxidizer_t {
//     float phi;
//     component_t* components_array;
//     size_t       components_count;
//     molecule_t   scf; // specific chemical formula
// } oxidizer_t;

typedef struct mixture_t {
    float km0;
    float km0_;
    component_t* oxidizer;
    component_t* fuel;
} mixture_t;

// typedef struct mixture_t {
//     component_t* components_array;
//     size_t components_count;
// } mixture_t;


#endif /* end of include guard: LAZURITE_TYPES_H */
