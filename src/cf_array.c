#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cf_atoms_params.h"
#include "cf_array.h"
#include "chemform.h"

static bounded_atom_t* array[ARRAY_SIZE];

size_t array_index = 0;

void array_init()
{
    for (size_t i = 0; i < ARRAY_SIZE; i++) {
        array[i] = bounded_atom_new();
    }
}

int comp (const void* ba1, const void* ba2)
{
    atom_t f = bounded_atom_get_atom(*(bounded_atom_t**)ba1);
    atom_t s = bounded_atom_get_atom(*(bounded_atom_t**)ba2);
    if (f > s) return  1;
    if (f < s) return -1;
    return 0;
}

void array_sort()
{
    qsort(array, array_index, sizeof(*array), comp);
}

// Нужно: переписать функцию array_collapse
// Слишком много вложенных циклов
void array_collapse(void)
{
    size_t k = 0;
    for (size_t i = 0; i < array_index;)
    {
        bounded_atom_t** sub_array = &array[i];
        float sum_index = bounded_atom_get_index(sub_array[0]);
        atom_t atom = bounded_atom_get_atom(sub_array[0]);

        size_t j = 1;
        while((atom == bounded_atom_get_atom(sub_array[j])) && (i+j < array_index))
        {
            sum_index += bounded_atom_get_index(sub_array[j]);
            j++;
        }

        bounded_atom_init(array[k], atom, sum_index);
        k++;

        i += j;
    }
    array_index = k;
}

void array_print()
{
    printf("> array: \n");
    for (size_t i = 0; i < array_index; i++) {
        bounded_atom_print(array[i]);
    }
    printf("> .\n");
}


void array_add(atom_t atom, float index)
{
    if (array_index == ARRAY_SIZE-1) {
        printf("array full\n");
        exit(-1);
    }
    bounded_atom_init(array[array_index], atom, index);
    array_index++;
}

void array_clear(void)
{
    array_index = 0;
}

size_t array_get_index(void)
{
    return array_index;
}

bounded_atom_t* array_get_element(int i)
{
    return array[i];
}

bounded_atom_t** array_get(void)
{
    return array;
}
