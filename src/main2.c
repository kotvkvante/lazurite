#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>


#include <string.h>

#include "chemform.h"
#include "cf_array.h"
#include "cf_parse.h"


int main(int argc, char const *argv[]) {
    UNUSED(argc);
    UNUSED(argv);
    // char* a0 = "H";
    // char* a1 = "C4";
    // char* a2 = "C4H";
    // char* a3 = "Cav6.4";
    // char* a4 = "Cav6,4";
    // char* a5 = "Cav6.4";
    // char* a6 = "Cav6.4.2";
    // parse_atom(a0);
    // parse_atom(a1);
    // parse_atom(a2);
    // parse_atom(a3);
    // parse_atom(a4);
    // parse_atom(a6);

    // char* res = parse_atom(a2);
    // res = parse_atom(res);


    // char* b0 = "(NH4)";
    // char* b1 = "(Al3H4)";
    // char* b2 = "(Al3H4";
    // char* b3 = "(Al3H4)2";
    // char* b4 = "(Al3(Ag2)H4(NH4)2)";
    // char* b5 = "(H4NH4)2";

    // parse_brackets(b0);
    // printf("\n");
    // parse_brackets(b1);
    // printf("\n");
    // // parse_brackets(b2);
    // parse_brackets(b3);
    // printf("\n");
    // // parse_brackets(b4);
    // parse_brackets(b5);
    // printf("\n");



    // atom_t o = atom_from_symbol("O");
    // printf("? o: %d\n", o == ATOM_OXYGEN);

    cf_init_default();

    molecule_t* m0 = molecule_from_str("H2SO4");
    molecule_print(m0);

    molecule_t* m1 = molecule_from_str("(AlH4)SO4");
    molecule_print(m1);

    molecule_t* m2 = molecule_from_str("Al2(SO4)3");
    molecule_print(m2);

    molecule_t* m3 = molecule_from_str("(NH4)2SO4");
    molecule_print(m3);
    //
    // molecule_t* nh4no3 = molecule_from_str("NH4NO3");
    // molecule_print(nh4no3);

    // parse_molecule("CH3CH2COOH");
    // array_sort();
    // array_collapse();
    // array_print(); array_clear();

    // parse_molecule("CH3COOH");
    // array_sort();
    // array_collapse();
    // array_print(); array_clear();
    //
    // parse_molecule("CH3CH2CH2CH3");
    // array_sort();
    // array_collapse();
    // array_print(); array_clear();
    //
    // parse_molecule("(CH3CO)2O");
    // array_sort();
    // array_collapse();
    // array_print(); array_clear();
    //
    // parse_molecule("(NH4)NO3");
    // array_sort();
    // array_collapse();
    // array_print(); array_clear();

    // parse_molecule("(NH4)NO3");
    // array_sort();
    // array_collapse();
    // array_print(); array_clear();
    //
    // parse_molecule("(NH4)NO3");
    // array_sort();
    // array_collapse();
    // array_print(); array_clear();

    // molecule_from_array();

    return 0;
}
