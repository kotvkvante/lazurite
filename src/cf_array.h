#ifndef LZ_STACK_H
#define LZ_STACK_H

#define ARRAY_SIZE 10

#include "cf_types.h"

void array_init(void);
void array_clear(void);
void array_sort(void);
void array_print(void);
void array_collapse(void);
void array_add(atom_t atom, float index);
size_t array_get_index(void);
bounded_atom_t** array_get(void);
// not safe, if i < 0 or i > array_index
bounded_atom_t* array_get_element(int i);

#endif /* end of include guard: LZ_STACK_H */
