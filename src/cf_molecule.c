#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "chemform.h"
#include "cf_types.h"
#include "cf_atoms_params.h"
#include "cf_parse.h"
#include "cf_array.h"

molecule_t* molecule_new()
{
    molecule_t* m = malloc(sizeof(molecule_t));

    m->str       = NULL;
    m->str_short = NULL;

    return m;
}

void molecule_init_c(molecule_t* m, size_t c)
{
    m->molar_mass = 0;
    m->bounded_atoms_count = c;
    m->bounded_atoms_array  = malloc(sizeof(bounded_atom_t) * c);
}

molecule_t*
molecule_from_ai_arrays(unsigned int count,
                        atom_t* atoms, float* indices)
{
    molecule_t* m = malloc(sizeof(molecule_t));

    m->molar_mass = 0;
    m->bounded_atoms_count = count;
    m->bounded_atoms_array  = malloc(sizeof(bounded_atom_t) * count);
    for (size_t i = 0; i < count; i++) {
        atom_t atom = atoms[i];
        if (NOT_ATOM > atom || atom > ATOMS_COUNT)
        {
            printf("No such atom\n");
            return NULL;
        }
        bounded_atom_init(&m->bounded_atoms_array[i], atom, indices[i]);
        m->molar_mass += ATOMIC_MASSES[atom] * indices[i];
    }

    return m;
}

molecule_t*
molecule_from_ba_array(unsigned int count,
                       bounded_atom_t** bounded_atoms_array)
{
    molecule_t* m = malloc(sizeof(molecule_t));
    m->molar_mass = 0;
    m->bounded_atoms_count = count;
    m->bounded_atoms_array  = malloc(sizeof(bounded_atom_t) * m->bounded_atoms_count);

    for (size_t i = 0; i < m->bounded_atoms_count; i++) {
        bounded_atom_init(
            &m->bounded_atoms_array[i],
            (bounded_atoms_array[i])->atom,
            (bounded_atoms_array[i])->index
        );
        m->molar_mass += ATOMIC_MASSES[m->bounded_atoms_array[i].atom] * m->bounded_atoms_array[i].index;
    }

    return m;
}

molecule_t* molecule_from_str(char* str)
{
    parse_molecule(str);
    array_sort();
    array_collapse();

    int count = array_get_index();
    bounded_atom_t** array = array_get();
    molecule_t* m = molecule_from_ba_array(count, array);
    m->str = str;

    array_clear();
    return m;
}

void
molecule_init_from_ba_parray(molecule_t* m, unsigned int c,
                             bounded_atom_t** ba_array)
{
    m->molar_mass = 0;
    m->bounded_atoms_count = c;
    m->bounded_atoms_array  = malloc(sizeof(bounded_atom_t) * c);

    for (size_t i = 0; i < c; i++) {
        bounded_atom_init(
            &m->bounded_atoms_array[i],
            (ba_array[i])->atom,
            (ba_array[i])->index
        );
        m->molar_mass += ATOMIC_MASSES[m->bounded_atoms_array[i].atom] * m->bounded_atoms_array[i].index;
    }
}

void
molecule_init_from_ba_array(molecule_t* m, unsigned int c,
                            bounded_atom_t* ba_array)
{
    m->molar_mass = 0;
    m->bounded_atoms_count = c;
    m->bounded_atoms_array  = malloc(sizeof(bounded_atom_t) * c);

    for (size_t i = 0; i < c; i++) {
        bounded_atom_init(
            &m->bounded_atoms_array[i],
            ba_array[i].atom,
            ba_array[i].index
        );
        m->molar_mass += ATOMIC_MASSES[m->bounded_atoms_array[i].atom] * m->bounded_atoms_array[i].index;
    }
}

void molecule_init_from_str(molecule_t* m, char* str)
{
    parse_molecule(str);
    array_sort();
    // array_print();
    array_collapse();
    // array_print();

    int count = array_get_index();
    bounded_atom_t** array = array_get();
    molecule_init_from_ba_parray(m, count, array);
    m->str = str;

    array_clear();
}

void molecule_init_from_m(molecule_t* m, molecule_t* src)
{
    molecule_init_from_ba_array(
        m,
        src->bounded_atoms_count,
        src->bounded_atoms_array
    );
}

atom_t molecule_get_atom_by_index(molecule_t* m, size_t index)
{
    return bounded_atom_get_atom(&m->bounded_atoms_array[index]);
}

void molecule_set_atom_by_index(molecule_t* m, size_t index, atom_t atom)
{
    m->bounded_atoms_array[index].atom = atom;
}

float molecule_calc_phi(molecule_t* m)
{
    float phi = 0;
    for (size_t i = 0; i < m->bounded_atoms_count; i++) {
        bounded_atom_t* ba = &(m->bounded_atoms_array[i]);
        phi += ba->oxidation_state * ba->index;
        // printf("[%d]- %f %d -> %f \n", ba->atom, ba->index, ba->oxidation_state, phi);
    }
    return phi;
}

float _km0(molecule_t* m1, molecule_t* m2)
{
    float phi1 = molecule_calc_phi(m1);
    float phi2 = molecule_calc_phi(m2);
    // printf("> phi1 = %f\n", phi1);
    // printf("> phi2 = %f\n", phi2);
    // (phi1 < 0 && phi2 > 0) || (phi1 > 0 && phi2 < 0)
    if (phi1 * phi2 >= 0)
    {
        return -1;
    }
    if (phi1 < 0)
    {
        return -phi2/phi1;
    }
    return -phi1/phi2;
}

float km0(molecule_t* m1, molecule_t* m2)
{
    // Потенциалы
    float phi1 = molecule_calc_phi(m1);
    float phi2 = molecule_calc_phi(m2);
    // printf("> phi1 = %f\n", phi1);
    // printf("> phi2 = %f\n", phi2);
    // (phi1 < 0 && phi2 > 0) || (phi1 > 0 && phi2 < 0)
    if (phi1 * phi2 >= 0)
    {
        // Нужно: обработка ошибки, когда невозможно
        // определить окислитель и горючее
        printf("> km0 Error :\n>\t Couldn't determine which element is the oxidizer and which is the fuel.\n");
        return -1;
    }
    if (phi1 < 0)
    {
        return -phi2/phi1 * m1->molar_mass / m2->molar_mass;
    }
    return -phi1/phi2 * m2->molar_mass / m1->molar_mass;
}

void molecule_print_short(molecule_t* m)
{
    char buffer[64];
    int n = 0;

    for (size_t i = 0; i < m->bounded_atoms_count; i++) {
            atom_t atom = m->bounded_atoms_array[i].atom;
            float index = m->bounded_atoms_array[i].index;

            n += sprintf(&buffer[n], "%s", ATOMIC_SYMBOLS[atom]);
            if (index != 1.0f)
            {
                n += sprintf(&buffer[n], "%g", index);
            }
        }
    printf("Molecule <short>: %s [%." MFPP "f]\n", buffer, m->molar_mass);

}

void molecule_print(molecule_t* m)
{
    char buffer[128];
    int n = 0;
    // if(m->str != NULL)
    //     { printf("Molecule: %s [%."MFPP"f]\n", m->str, m->molar_mass); return; }
    // if(m->str_short != NULL)
    //     { printf("Molecule: %s [%."MFPP"f]\n", m->str_short, m->molar_mass); return; }

    for (size_t i = 0; i < m->bounded_atoms_count; i++) {
        atom_t atom = m->bounded_atoms_array[i].atom;
        float index = m->bounded_atoms_array[i].index;

        n += sprintf(&buffer[n], " %s", ATOMIC_SYMBOLS[atom]);
        if (index != 1)
        {
            n += sprintf(&buffer[n], "%." MFPP "f", index);
        }
    }

    printf("Molecule: %s [%."MFPP"f]\n", buffer, m->molar_mass);
}
