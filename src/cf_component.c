#include <stdlib.h>
#include <stdio.h>

#include "cf_types.h"
#include "cf_atoms_params.h"
#include "chemform.h"

// conventional molar mass
// условная молярная масса
float g_cmm = 100.0f;

component_t* component_new()
{
    return malloc(sizeof(component_t));
}

void component_init_from_s_mf(component_t* comp, size_t c, char** strs, float* mf)
{
    comp->molecules_count = c;
    comp->molecules_array = malloc(sizeof(molecule_t*) * c);
    comp->masses_fraction = malloc(sizeof(float) * c);

    // Проверка суммы массовых долей компонентов.
    float t = 0;

    for (size_t i = 0; i < c; i++) {
        comp->molecules_array[i] = molecule_new();
        molecule_init_from_str(comp->molecules_array[i], strs[i]);
        comp->masses_fraction[i] = mf[i];
        t += mf[i];
    }

    if (t != 1.0f) { printf("> Warning: Masses fraction sum = %f ( != 1.0f)\n", t); }
}

void component_init_from_m_mf(component_t* comp, size_t c, molecule_t** m, float* mf)
{
    comp->molecules_count = c;
    comp->molecules_array = malloc(sizeof(molecule_t*) * c);
    comp->masses_fraction = malloc(sizeof(float) * c);

    float t = 0;
    for (size_t i = 0; i < c; i++) {
        comp->molecules_array[i] = m[i];
        comp->masses_fraction[i] = mf[i];
        t += mf[i];
    }

    if (t != 1.0f) { printf("> Warning: Masses fraction sum = %f ( != 1.0f)\n", t); }
}

size_t component_union(molecule_t* result, molecule_t* m1, molecule_t* m2)
{
    size_t n1 = m1->bounded_atoms_count;
    size_t n2 = m2->bounded_atoms_count;

    size_t i = 0, j = 0, k = 0;
    while((i < n1) && (j < n2)) {
        atom_t o1 = molecule_get_atom_by_index(m1, i);
        atom_t o2 = molecule_get_atom_by_index(m2, j);
        if (o1 == o2) { molecule_set_atom_by_index(result, k, o1); i++; j++; k++; }
        else if (o1 < o2) { molecule_set_atom_by_index(result, k, o1); i++; k++; }
        else if (o1 > o2) { molecule_set_atom_by_index(result, k, o2); j++; k++; }
    }

    while (i < n1) {
        atom_t o1 = molecule_get_atom_by_index(m1, i);
        molecule_set_atom_by_index(result, k, o1); i++; k++;
    }

    while (j < n2) {
        atom_t o2 = molecule_get_atom_by_index(m2, j);
        molecule_set_atom_by_index(result, k, o2); j++; k++;
    }

    return k;
}

void component_union_molecules(component_t* comp)
{
    bounded_atom_t ba1[MOLECULE_UNION_MAX_ELEMENTS] = {0};
    molecule_t m1 = { .bounded_atoms_count = 0, .bounded_atoms_array = ba1
    };

    bounded_atom_t ba2[MOLECULE_UNION_MAX_ELEMENTS] = {0};
    molecule_t m2 = {
        .bounded_atoms_count = 0,
        .bounded_atoms_array = ba2
    };

    molecule_t* result = NULL;
    molecule_t* tmp1 = &m1;
    molecule_t* tmp2 = &m2;

    size_t i = 0;
    size_t n = 0;
    while (i < comp->molecules_count)
    {
        n = component_union(tmp1, tmp2, comp->molecules_array[i]);
        tmp1->bounded_atoms_count = n;
        result = tmp1;
        tmp1 = tmp2;
        tmp2 = result;
        i++;
    }

    molecule_init_from_ba_array(&comp->scf, n, result->bounded_atoms_array);
}

void component_calc_scf(component_t* comp)
{
    component_union_molecules(comp);

    size_t rs = comp->scf.bounded_atoms_count;
    float* res = malloc(sizeof(float) * rs);

    size_t n = comp->molecules_count * rs;
    float* arr = malloc(sizeof(float) * n);

    for (size_t i = 0; i < n; i++) arr[i] = 0;
    for (size_t i = 0; i < rs; i++) res[i] = 0;

    for (size_t i = 0; i < comp->molecules_count; i++)
    {
        float mm = comp->molecules_array[i]->molar_mass;
        size_t k = 0, j = 0;
        while(k < comp->molecules_array[i]->bounded_atoms_count)
        {
            if (comp->molecules_array[i]->bounded_atoms_array[k].atom != comp->scf.bounded_atoms_array[j].atom)
                j++;
            else
            {
                atom_t atom = comp->molecules_array[i]->bounded_atoms_array[k].atom;
                float index = comp->molecules_array[i]->bounded_atoms_array[k].index;
                arr[rs * i + j] =  index * ATOMIC_MASSES[atom] / mm;
                k++;
            }
        }
    }

    for (size_t i = 0; i < comp->scf.bounded_atoms_count; i++) {
        for (size_t j = 0; j < comp->molecules_count; j++) {
            res[i] += comp->masses_fraction[j] * arr[j * comp->scf.bounded_atoms_count + i];
        }
    }

    float cmm = g_cmm;
    for (size_t i = 0; i < comp->scf.bounded_atoms_count; i++) {
        comp->scf.bounded_atoms_array[i].index = res[i] * cmm / ATOMIC_MASSES[comp->scf.bounded_atoms_array[i].atom];
    }
    comp->scf.molar_mass = cmm;

    free(arr);
    free(res);
}

void component_print(component_t* comp)
{
    printf("> Component: \n");
    for (size_t i = 0; i < comp->molecules_count; i++) {
        printf("g=[%.2f] ", comp->masses_fraction[i]);
        molecule_print(comp->molecules_array[i]);
        // printf(">\t %s [%f]\n", comp->molecules_array[i]->str, );
    }
}

void component_print_scf(component_t* comp)
{
    printf("> Component SCF: \n>\t");
    molecule_print(&comp->scf);
}

molecule_t* component_get_scf(component_t* comp)
{
    return &comp->scf;
}
