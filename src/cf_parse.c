#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

#include "chemform.h"
#include "cf_array.h"

char* parse_index(char* f, float* index)
{
    char* p = f;
    if (isdigit(*p))
    {
        *index=strtof(p, &p);
    }
    else
    {
        *index=1.0f;
    }
    // printf("(%f)", *index);
    return p;
}

char* parse_atom(char* f)
{
    int i = 1;
    char* p = f;
    char buffer[3];
    float index;
    // printf("\"%s\"", p); // вывести обрабатываемую строку
    p++;
    // printf("%c ", *p);
    while(islower(*p) && (*p != '\0'))
    {
        i++;
        p++;
    }
    if (i > 2) { printf("Invalid Atom: %-.*s\n", i, f); exit(-1); }
    // printf("=> %-.*s \n", i, f); // вывести определённый символ атома
    p = parse_index(p, &index);

    strncpy(buffer, f, i);
    buffer[i]='\0'; // strncpy is not null terminated

    atom_t atom = atom_from_symbol(buffer);
    // Нужно: Добавить проверку и вывод информации
    // об ошибке определения нормера атома.
    if (atom == NOT_ATOM)
    {
        printf("Failed to get atom from symbol: %s.\n", buffer);
        exit(-1);
    }
    array_add(atom, index);

    return p;
}

char* parse_brackets(char* f)
{
    char* p = f;
    float index;
    // Запоминаем начальную позицию списка, для того
    // чтобы пересчитать число атомов в скобках
    // Al2(SO4)3 => 12 O, 3 S
    int start = array_get_index();

    // Пропустить первую скобку
    p++;
    while(*p !='\0')
    {
        if (*p == '(')
        {
            p = parse_brackets(p);
        }

        p = parse_atom(p);

        if (*p == ')')
        {
            p++;
            p = parse_index(p, &index);

            for (size_t i = start; i < array_get_index(); i++) {
                bounded_atom_t* ba = array_get_element(i);
                bounded_atom_multiply(ba, index);
            }
            return p;
        }
    }
    // Нужно: добавить сообщение ошибки
    // об отсутствии закрывающейся скобки
    printf("No matching ')'!");

    return p;
}

void parse_molecule(char* f)
{
    // printf("Molecule to parse: %s\n", f);
    char* p = f;
    while(*p != '\0')
    {
        if (*p == '(')
        {
            p = parse_brackets(p);
        }
        else
        {
            p = parse_atom(p);
        }
    }
}
