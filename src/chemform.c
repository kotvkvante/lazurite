#include <stdio.h>
#include <string.h>

#include "cf_array.h"
#include "cf_types.h"
#include "chemform.h"

#define ATOMS_PARAMS_IMPL
#include "cf_atoms_params.h"

extern float g_cmm;

void cf_init_default(void)
{
    array_init();

    cf_set_default_atomic_masses();
    cf_set_default_oxidation_states();
}

void cf_set_atomic_mass(atom_t atom, float atomic_mass)
{
    ATOMIC_MASSES[atom] = atomic_mass;
}

void cf_set_atomic_masses(float **atomic_masses)
{
    for (size_t i = 0; i < ATOMS_COUNT; i++) {
        ATOMIC_MASSES[i] = (*atomic_masses)[i];
    }
}

void cf_set_default_atomic_masses(void)
{
    for (size_t i = 0; i < ATOMS_COUNT; i++) {
        ATOMIC_MASSES[i] = DEFAULT_ATOMIC_MASSES[i];
    }
}

void cf_set_oxidation_state(atom_t a, float os)
{
    OXIDATION_STATES[a] = os;
}

void cf_set_oxidation_states(float **os)
{
    for (size_t i = 0; i < ATOMS_COUNT; i++) {
        OXIDATION_STATES[i] = (*os)[i];
    }
}

void cf_set_default_oxidation_states(void)
{
    for (size_t i = 0; i < ATOMS_COUNT; i++) {
        OXIDATION_STATES[i] = DEFAULT_OXIDATION_STATES[i];
    }
}

void cf_set_cmm(float cmm)
{
    if (cmm < 0) {
        printf("> Warning: cmm < 0. Given %f\n", cmm);
        return;
    }
    g_cmm = cmm;
}

float cf_get_cmm(void)
{
    return g_cmm;
}


float cf_calc_km0_mm(molecule_t* m1, molecule_t* m2)
{
    return km0(m1, m2);
}

float cf_calc_km0_cc(component_t* c1, component_t* c2)
{
    molecule_t* m1 = component_get_scf(c1);
    molecule_t* m2 = component_get_scf(c2);
    return km0(m1, m2);
}

atom_t atom_from_symbol(char* str)
{
    for (size_t i = NOT_ATOM; i < ATOMS_COUNT; i++) {
        if (strcmp(str, ATOMIC_SYMBOLS[i]) == 0) return i;
    }
    return NOT_ATOM;
}

bounded_atom_t* bounded_atom_new(void)
{
    return malloc(sizeof(bounded_atom_t));
}

void bounded_atom_init(bounded_atom_t* bounded_atom, atom_t atom, float index)
{
    bounded_atom->atom = atom;
    bounded_atom->index = index;
    bounded_atom->oxidation_state = OXIDATION_STATES[atom];
}

atom_t bounded_atom_get_atom(bounded_atom_t* bounded_atom)
{
    return bounded_atom->atom;
}

float bounded_atom_get_index(bounded_atom_t* ba)
{
    return ba->index;
}

// bool bounded_atom_is_equal(bounded_atom_t* ba1, bounded_atom_t* ba2)
// {
//     return ba1->atom == ba2->atom;
// }

// void bounded_atom_add_real(bounded_atom_t* ba1, bounded_atom_t* ba2)
// {
//     // if (!bounded_atom_is_equal(ba1, ba2)) return;
//     // Нужно: возмоджно необходимо учесть разные степени окисления.
//     ba1->index += ba2->index;
// }

void bounded_atom_multiply(bounded_atom_t* bounded_atom, float factor)
{
    bounded_atom->index = bounded_atom->index * factor;
}

void bounded_atom_print(bounded_atom_t* bounded_atom)
{
    printf("> Bounded Atom: %s %f\n", ATOMIC_SYMBOLS[bounded_atom->atom], bounded_atom->index);
}
