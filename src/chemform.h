/**

@author kotvkvante
@version 0.1
@brief Library for processing chemical formulas
@details Some explanations..
@date 2023
@bug there is no memeory cleaning for now..

@mainpage The ChemForm library
@section intro_sec Introduction
this is intro of code
@section compile_sec Compilation
you have to use your hands to compile this stuff
@subsection Step 1
yep, use it with keyboard
@subsection Step 2
probably you will need a computer..

*/


#ifndef CHEMFORM_H
#define CHEMFORM_H

#include <stddef.h>
#include <stdint.h>

#include "cf_atoms.h"

#define DAOS OXIDATION_STATES


#define MOLECULE_UNION_MAX_ELEMENTS 16
// Molecule float print precision
#define MFPP "2"

#define UNUSED(x) (void)(x)

typedef struct bounded_atom_t bounded_atom_t;
typedef struct molecule_t molecule_t;
typedef struct component_t component_t;
typedef struct fuel_t fuel_t;
typedef struct mixture_t mixture_t;

/*!
   \brief Init default ChemForm state
*/
void cf_init_default(void);

/*!
   \brief Set atomic mass for given atom
   @param atom Atom
   @param atomic_mass Atomic Mass
*/
void cf_set_atomic_mass(atom_t atom, float atomic_mass);

/*!
   \brief Задать атомные массы нескольких атомов
   @param atomic_masses Указатель на массив атомных масс, где
   индекс элемента равен его порядковому номеру в периодической таблице элементов
*/
void cf_set_atomic_masses(float **atomic_masses);

void cf_set_default_atomic_masses(void);

void cf_set_oxidation_state(atom_t a, float os);
void cf_set_oxidation_states(float **os);
void cf_set_default_oxidation_states(void);

float cf_calc_km0_mm(molecule_t* m1, molecule_t* m2);
float cf_calc_km0_cc(component_t* c1, component_t* c2);

/// @brief Устанавливает условную молярную массу
/// для расчёта удельной химической формулы
void cf_set_cmm(float cmm);

/// @brief Возвращает текущее значение условной молярной массы,
/// которое будет использоваться в расчётах
float cf_get_cmm(void);

/**
@return atom_t
@param str Pointer to the c string which is contain atom symbol.
*/
atom_t atom_from_symbol(char* str);

bounded_atom_t* bounded_atom_new(void);

void bounded_atom_init(bounded_atom_t* bounded_atom, atom_t atom, float index);
atom_t bounded_atom_get_atom(bounded_atom_t* ba);
float bounded_atom_get_index(bounded_atom_t* ba);
// bool bounded_atom_is_equal(bounded_atom_t* ba1, bounded_atom_t* ba2);

void bounded_atom_multiply(bounded_atom_t* bounded_atom, float factor);
void bounded_atom_print(bounded_atom_t* bounded_atom);


molecule_t* molecule_new();
void molecule_init_c(molecule_t* m, size_t c);
// Нужно: переимновать функции _from_ в _init_from_
// Нужно: new -> init -> free
molecule_t* molecule_from_ai_arrays(unsigned int count, atom_t* atoms, float* indices);
molecule_t* molecule_from_ba_array(unsigned int count, bounded_atom_t** bounded_atoms_array);
molecule_t* molecule_from_str(char* str);


void molecule_init_from_ba_parray(molecule_t* m, unsigned int c, bounded_atom_t** ba_array);
void molecule_init_from_ba_array(molecule_t* m, unsigned int c, bounded_atom_t* ba_array);
void molecule_init_from_str(molecule_t* m, char* str);
void molecule_init_from_m(molecule_t* m, molecule_t* src);

// Not safe if index out of array range
void molecule_set_atom_by_index(molecule_t* m, size_t index, atom_t atom);
// Not safe if index out of array range
atom_t molecule_get_atom_by_index(molecule_t* m, size_t index);


float molecule_calc_phi(molecule_t* m);
// void molecule_union(molecule_t* result, molecule_t* m1, molecule_t* m2);

float _km0(molecule_t* m1, molecule_t* m2);
float km0(molecule_t* m1, molecule_t* m2);

void molecule_print_short(molecule_t* m);
void molecule_print(molecule_t* m);

// Component
component_t* component_new();
void component_init(component_t* comp, molecule_t* m, float mf);
void component_init_from_s_mf(component_t* comp, size_t c, char** strs, float* mf);
void component_init_from_m_mf(component_t* comp, size_t c, molecule_t** m, float* mf);


size_t component_union(molecule_t* result, molecule_t* m1, molecule_t* m2);
void component_union_molecules(component_t* comp);
void component_calc_scf(component_t* comp);
void component_print(component_t* comp);
void component_print_scf(component_t* comp);

molecule_t* component_get_scf(component_t* comp);


// Fuel
// fuel_t* fuel_new();
// void fuel_init(fuel_t* fl);
// void fuel_init_from_components(fuel_t* fl, size_t c, component_t* comp);
// void fuel_init_from_molecules(fuel_t* fl, size_t c, molecule_t* m, float* mf);
// void fuel_init_from_str_mf(fuel_t* fl, size_t c, char** strs, float* mf);

// Oxidizer
// oxidizer_t* oxidizer_new();
// void oxidizer_init_from_components(oxidizer_t* ox, size_t c, component_t* comp);
// void oxidizer_init_from_str_mf(oxidizer_t* ox, size_t c, char** strs, float* mf);
// void oxidizer_print(oxidizer_t* ox);



void fuel_print(fuel_t* fl);
// Mixture
mixture_t* mixture_new();
void mixture_from_arrays_mf(size_t count, molecule_t* molecules, float* mass_fractions);

#endif /* end of include guard: CHEMFORM_H */
