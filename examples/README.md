# Build:
You can build examples after compiling chemform library.
Open _build directory:
```
cd examples
make
```  
# Usage
## calc_km0
```
./calc_km0
```
Then enter task parameters..

## 3components
```
./3components --help
./3components -?
```
```
./3components -f "CH4 O2 H2" -m "0.3 0.5 0.2"
./3components -f "HNO3 C8H14 Al" -m "0.55 0.35 0.1"
./3components -f "HNO3 C8H14, Al" -m "0.55 0.35, 0.1" -c 1000.0
```
