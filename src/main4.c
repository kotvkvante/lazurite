#include <stdlib.h>
#include <stdio.h>

#include "chemform.h"

int main(int argc, char const *argv[]) {
    UNUSED(argc);
    UNUSED(argv);

    cf_init_default();

    cf_set_oxidation_state(ATOM_OXYGEN, 2);
    molecule_t* h2o = molecule_new();
        molecule_init_from_str(h2o, "H2O");
        molecule_print(h2o);


    molecule_t* f2 = molecule_new();
        molecule_init_from_str(f2, "F2");
        molecule_print(f2);

    float km0_h2of2 = km0(h2o, f2);

    printf("Km0 = %f\n", km0_h2of2);

    return 0;
}
